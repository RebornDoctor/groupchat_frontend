import group01 from './group01.jpg'; 
import group02 from './group02.jpg'; 
import group03 from './group03.jpg'; 
import groupInfo from './groupInfo.jpg'; 

export {
  group01 ,
  group02,
  group03,
  groupInfo
}