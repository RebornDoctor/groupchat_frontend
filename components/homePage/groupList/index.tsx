import { group02 } from '@/assets/images'
import GroupItem from './groupItem'

function GroupList() {

    return (
        <>
            <GroupItem imgSrc={group02} />
        </>
    )
}

export default GroupList