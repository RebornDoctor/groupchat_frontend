import { group01 } from '@/assets/images'
import Image, { StaticImageData } from 'next/image'
import Link from 'next/link'
import React from 'react'

function GroupItem({ imgSrc }: { imgSrc?: StaticImageData }) {

    return (
        <div className="my-3  ">
            <div className="bg-neutral-700 hover:bg-neutral-600 p-4 rounded-md">
                <div className="flex items-center flex-wrap gap-2 justify-between">
                    <Link href={"/55"} className="flex gap-4 items-center">
                        <Image
                            src={imgSrc || group01}
                            alt="group image"
                            height={60}
                            width={60}
                            className="object-cover rounded-full group__img shrink-0"
                        />
                        <div>
                            <h2 className="font-bold">Group Name</h2>
                            <p className="text-sm max-w-[300px]">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </Link>
                    <div className="ms-auto">
                        <Link
                            href="/65/chat"
                            className="bg-neutral-500 px-4 py-2 text-sm rounded-md"
                        >
                            JOIN
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GroupItem