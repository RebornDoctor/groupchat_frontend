import React from 'react'

function SearchBox() {
    return (
        <div className="container py-3">
            <input
                type="text"
                placeholder="search"
                className="p-2 px-3 bg-neutral-600 rounded-md w-full"
            />
        </div>
    )
}

export default SearchBox