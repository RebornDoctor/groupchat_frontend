import Image from 'next/image'
import { group01 } from '@/assets/images'
import React from 'react'

function GroupInfo() {
    return (
        <div className="flex gap-3 items-center">
            <Image
                src={group01}
                alt="group image"
                height={60}
                width={60}
                className="object-cover rounded-full chat__img shrink-0"
            />
            <h3 className="font-bold text-2xl">Honda</h3>
        </div>
    )
}

export default GroupInfo