import ChatBubble from "./chatBubble"

function ChatList() {
    return (
        <>
            <ChatBubble isSender={true} />
            <ChatBubble isSender={false} />
            <ChatBubble isSender={true} />
            <ChatBubble isSender={false} />
        </>
    )
}

export default ChatList