import { group01, group02 } from "@/assets/images";
import Image from "next/image";
import React from "react";

const ChatBubble = ({ isSender }: { isSender: boolean }) => {

    return (
        <>

            <div className={`flex items-start mb-3 gap-3 ${isSender ? '' : 'flex-row-reverse'}`}>
                <Image
                    src={group01}
                    alt="group image"
                    height={60}
                    width={60}
                    className="object-cover rounded-full chat__img shrink-0"
                />
                <div className={`chat__message p-3 px-4 ${isSender ? 'text-slate-500 bg-slate-500 chat--left' : 'text-slate-700 bg-slate-700 text-end chat--right'}`}>
                    <h3 className="font-black text-white">Ahmed Sokkar</h3>
                    <p className="chat__message__content text-white max-w-[500px]">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </p>
                    <p className="chat__message__time text-white text-sm">09:00 AM</p>
                </div>
            </div>
        </>
    );
};

export default ChatBubble;
