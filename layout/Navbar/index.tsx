import Link from "next/link";

const Navbar = () => {
  return (
    <div className="py-4 bg-zinc-900 fixed top-0 w-full z-40">
      <div className="container">
        <Link href={"/"} className="font-black text-lg" prefetch={false}>
          GroupApp
        </Link>
      </div>
    </div>
  );
};

export default Navbar;
