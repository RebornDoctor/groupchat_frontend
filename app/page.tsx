import GroupList from "@/components/homePage/groupList";
import SearchBox from "@/components/homePage/searchBox";

export default function Home() {
  return (
    <main className="my-4 relative container">
      <div className=" fixed top-[60px] bg-neutral-800 left-0 right-0">
        <SearchBox />
      </div>
      <div className=" pt-[120px]">
        <h3 className="pt-4 font-bold text-xl">Groups</h3>
        <GroupList />
      </div>
    </main>
  );
}
