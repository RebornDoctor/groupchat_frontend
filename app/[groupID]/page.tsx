import { groupInfo } from "@/assets/images";
import Image from "next/image";
import Link from "next/link";
import React from "react";

const GroupPreview = () => {
  return (
    <div className="pt-[80px]">
      <div className="container pt-5 flex gap-5 items-center bg-neutral-700 p-5 rounded-md ">
        <Image src={groupInfo} alt="picture" className="group__info__img" />

        <div className="">
          <h2 className="text-2xl font-bold mb-1">Group Name</h2>
          <p className="max-w-[550px] text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
            esse quo neque nisi laboriosam enim velit, eius impedit consequuntur
            maxime perspiciatis est ut voluptas corrupti saepe expedita. Sed,
            est voluptas.
          </p>
        </div>
        <div className="ms-auto">
          <Link
            href={"/45/chat"}
            className="bg-neutral-500 px-7 py-2 text-sm rounded-md"
          >
            JOIN
          </Link>
        </div>
      </div>
    </div>
  );
};

export default GroupPreview;
