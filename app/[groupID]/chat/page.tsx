import ChatList from "@/components/chattingPage/chatList";
import GroupInfo from "@/components/chattingPage/groupInfo";

const ChatContainer = () => {
  return (
    <div className="mt-[80px]">
      <div className="container bg-neutral-600 rounded-md ">
        <div className="bg-neutral-700 py-3 px-5">
          <GroupInfo />
        </div>
        <div className="chat h-[65vh] flex flex-col  justify-end ">
          <div className="overflow-y-auto h-full p-5">
            <ChatList />
          </div>
        </div>
        <div className="bg-neutral-500 flex gap-3 p-5">
          <input
            type="text"
            className="w-full rounded-md bg-neutral-600 p-2 px-4"
            placeholder="type your message"
          />
          <button className="bg-zinc-600 px-4 rounded-lg py-2">Send</button>
        </div>
      </div>
    </div>
  );
};

export default ChatContainer;
